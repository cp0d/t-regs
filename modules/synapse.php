<?php

/*
 * t-regs module for registering accounts on
 * synapse matrix server.
 */

define("TREGS_SYNAPSE_VERSION",		"v0.1");
define("SYNAPSE_API_ENDPOINT_USERS",	"/_synapse/admin/v2/users/");

function register_synapse($username, $password) {
	require(__DIR__ . '/synapse.conf.php');
	$user_id = "@" . $username . ":" . $mod_cfg['matrix_server_name'];
	$curl_handle = curl_init();
	# check if user account already exists
	$curl_options = array(
		CURLOPT_HTTPGET => true,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_USERAGENT => "t-regs synapse " . TREGS_SYNAPSE_VERSION,
		CURLOPT_HTTPHEADER => [ "Authorization: Bearer " . $mod_cfg['admin_token'] ],
		CURLOPT_URL => $mod_cfg['api_host'] . SYNAPSE_API_ENDPOINT_USERS . $user_id,
	);
	if (curl_setopt_array($curl_handle, $curl_options) === false) {
		$msg = "Failed to check if user '" . $username . "' already exists!<br>curl_setopt: " . curl_error($curl_handle);
		curl_close($curl_handle);
		return array(false, $msg);
	}
	
	if (($response = curl_exec($curl_handle)) === false) {
		$msg = "Failed to check if user '" . $username . "' already exists!<br>curl_exec: " . curl_error($curl_handle);
		curl_close($curl_handle);
		return array(false, $msg);
	}
	$user = json_decode($response, $associative = true);
	if (array_key_exists("name", $user)) {
		$msg = "User '" . $username . "' already exists!";
		curl_close($curl_handle);
		return array(false, $msg);
	}
	if (array_key_exists("errcode", $user) and strcmp($user['errcode'], "M_NOT_FOUND") != 0) {
		$msg = "Failed to register user '" . $username . "'!<br>Unexpected response from server, error code: " . $user['errcode'];
		curl_close($curl_handle);
		return array(false, $msg);
	}
	curl_reset($curl_handle);

	# register user
	$user_data = array(
		"password" => $password,
		"displayname" => $username,
		"admin" => false,
		"deactivated" => false,
	);
	$json_data = json_encode($user_data);
	$curl_options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_USERAGENT => "t-regs synapse " . TREGS_SYNAPSE_VERSION,
		CURLOPT_HTTPHEADER => array("Content-Type: application/json", "Authorization: Bearer " . $mod_cfg['admin_token']),
		CURLOPT_CUSTOMREQUEST => "PUT",
		CURLOPT_POSTFIELDS => $json_data,
		CURLOPT_URL => $mod_cfg['api_host'] . SYNAPSE_API_ENDPOINT_USERS . $user_id,
	);
	if (curl_setopt_array($curl_handle, $curl_options) === false) {
		$msg = "Failed to register user '" . $username . "'!<br>curl_setopt: " . curl_error($curl_handle);
		curl_close($curl_handle);
		return array(false, $msg);
	}
	if (($response = curl_exec($curl_handle)) === false) {
		$msg = "Failed to register user '" . $username . "'!<br>curl_exec: " . curl_error($curl_handle);
		curl_close($curl_handle);
		return array(false, $msg);
	}

	$msg = "User " . $user_id . " has been registered, please log in to " . $mod_cfg['homeserver_hostname'];
	curl_close($curl_handle);
	return array(true, $msg);
}

?>
