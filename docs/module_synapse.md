# Module _synapse_

The module _synapse_ provides registration of matrix account for [Synapse](https://github.com/matrix-org/synapse) homeservers.
It uses Synapse's [admin API](https://github.com/matrix-org/synapse/tree/develop/docs/admin_api) to create accounts.

The following configuration options need to be set:
- 'matrix_server_name': The last part of the homeserver's user ID's (the part following the colon).
- 'homeserver_hostname': The homeserver's actual hostname.
- 'api_host': The hostname the that provides the API endpoints, usually on localhost.
- 'admin_token': An access token of a server admin required to interact with the admin API.
- 'tokenfile': The text file containing the actual tokens.
As with other modules, a 'password_policy' may additionally be set to define the password complexity requirements.
