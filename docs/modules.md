# Modules

_t_regs_ relies on modules to perform the actual user registration.
For each application, a separate module is required to communicate with this application's user registration facilities.
The standard installation already comes with a module to register users with [Synapse](https://github.com/matrix-org/synapse), the Matrix server reference implementation.
Additional modules can be created using the _Module API_.

## Module API

Modules consist of two PHP files inside the 'module' directory:
- _modulename_.php: The module function file.
- _modulename_.conf.php: The module configuration file.

The module function file implements a function named 'register_modulename_' with the following signature:
```php
register_modulename($token, $username, $password)
{
	/* account registration code here */
	return array($success, $message);
}
```
The return value expected is an array consisting of a boolean value indicating if account registration was successful an a message.
This message is expected to comtain information about the account created if registration was successful, an error message otherwise.

The module configuration file must be named '_modulename_.conf.php' and is expected to implement an array named '$mod_cfg', which contains configuration key-value-pairs for the specific module.
This array must provide at least the following keys:
- 'tokenfile': Path to a file storing the tokens for this module.
- 'password_policy': Flags describing the password policy (see section _Password Policy_)

## Using Modules

Modules are registered to _t-regs_ by including the _modulename_ in the 'modules' configuration value array in _t-regs_' 'config.php'.
