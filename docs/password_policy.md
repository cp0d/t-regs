# Password Policy

For account registration, a password policy is required wihich sets rules for the minimum password complexity.
This password policy is described by a string containing flags defining a number specifying minimum lenght and a combination of optional flags:
- 'l': must contain a lower-case character
- 'u': must contain an upper-case character
- 'n': must contain a number
- '?': must contain a special character

If unset, the policy string defaults to '10lun?': 10 charaters minimum, must contain lower-case, upper-case and special characters and a number.
The password policy is set per module.
