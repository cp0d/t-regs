# Token File

The tokens authorizing users to register accounts are kept in a separate token file per module.
Each token enables a user to register exactlz one new account.
The token files _must not_ be stored within the _t-regs_ web root.

The token file follows a simple syntax:
Each line consists of a pipe-separated entry specifying the token and a comment.
Additionally, a creation and an expiration date may be added.
These dates are not yet used by _t-regs_.
While this may be the case in the future, these dates could also be used to delete expired tokens via cron jobs.

Tokens should only consist of printable ASCII characters with no further restriction otherwise (beside the obvious restriction to not contain any pipe symbols).
In general, tokens should be complex enough not to be guessable easily.

Blank lines in the token files are ignored.
As the token itself is not restricted within the ASCII printable character set, no comments exist in token files.
