# t-regs

**t-regs** is a simple web application for token-based user registration.
It provides a web portal for the registration of user accounts for different applications to users that can provide a token authorizing them to register said account.
_t_regs_ provides generic creation of accounts consisting of username and password by relying on application-specific modules for the actual account creation.
Currently, the base installation of **t-regs** comes with plugins for for the following applications:
- Synapse (the Matrix server reference implementation)

## Quickstart

- Copy this reposiroty's content into the desired domain's web root.
- Copy the t-regs' _config.php.example_ to _config.php_ and change the relevant values.
- For each module to be used:
  - Copy the module's example config file and change the relevant values.
  - Add the module in the t-regs _config.php_ to the config value `modules`.
  - Create the [token file](docs/token_file.md) pointed to in the module config and create tokens.
  - Start handing out tokens.

## Configuration

Generic configuration for the registration portal itself happens in _config.php_.
Additionally, each module comes with its own configuration file inside the _modules_ directory.

## Contact

- Email: praty.code@0xaa55.org
- Matrix: #praty_code:0xaa55.org
