<?php
require('config.php');

# config check
if (count($cfg['modules']) == 0) {
	$_error[] = "No registration modules loaded!";
} else {
	if ($_POST['username']) {
		$_error = array();
		# check if token is provided
		if (!$_POST['token']) {
			$_error[] = "Token missing!";
		} else {
			# check which module to use
			if (!in_array($_POST['module'], array_keys($cfg['modules']))) {
				$_error[] = "Invalid registration module '" . $_POST['module'] . "' requested!";
			} else {
				if (!is_readable('modules/' . $_POST['module'] . ".php")) {
					$_error[] = "Configuration error: Registration module file 'modules/" . $_POST['module'] . ".php' is unreadable!";
				} else {
					require("modules/" . $_POST['module'] . ".php");
					require("modules/" . $_POST['module'] . ".conf.php");
					# check if password and confirmations match
					if (strcmp($_POST['password'], $_POST['password_confirm']) <> 0) {
						$_error[] = "Password and confirmation do not match!";
					} else {
						# check password agains policy
						$password_policy = $mod_cfg['password_policy'] ?$mod_cfg['password_policy'] :"10lun?";
						$minlength = (int)$password_policy;
						if (strlen($_POST['password']) < $minlength) {
							$_error[] = "Password must be at least $minlength characters long";
						}
						if (strstr($password_policy, 'l') && preg_match('/[a-z]/', $_POST['password']) == 0) {
							$_error[] = "Password must contain at least one lower-case character!";
						}
						if (strstr($password_policy, 'u') && preg_match('/[A-Z]/', $_POST['password']) == 0) {
							$_error[] = "Password must contain at least one upper-case character!";
						}
						if (strstr($password_policy, 'n') && preg_match('/[0-9]/', $_POST['password']) == 0) {
							$_error[] = "Password must contain at least one number!";
						}
						if (strstr($password_policy, '?') && preg_match('/[^a-zA-Z0-9]/', $_POST['password']) == 0) {
							$_error[] = "Password must contain at least one special character!";
						}
					} # password confirmation check
					# if password is compliant, check token
					if (count($_error) == 0) {
						if (!is_readable($mod_cfg['tokenfile'])) {
							$_error[] = "Unable to read token file!";
						} else {
							$token_lines = file($mod_cfg['tokenfile'], FILE_SKIP_EMPTY_LINES);
							$token = "";
							$token_found = false;
							foreach ($token_lines as $token_id => $token_line) {
								if (strcmp($token = trim(explode("|", $token_line)[0]), $_POST['token']) == 0) {
									unset($token_lines[$token_id]);
									$token_found = true;
									break;
								}
							}
							if ($token_found) {
								# remove token from file
								if (!is_writable($mod_cfg['tokenfile'])) {
									$_error[] = "Unable to write token file!";
								} else {
									# register user
									if (!is_callable('register_' . $_POST['module'])) {
										$_error[] = "Configuration error: Cannot call function 'register_" . $_POST['module'] . "'!";
									} else {
										list ($success, $msg) = call_user_func('register_' . $_POST['module'],  $_POST['username'], $_POST['password']);
										if ($success) {
											$_success = $msg;
											# remove token from token file
											file_put_contents($mod_cfg['tokenfile'], $token_lines);
											# unset POST variables
											unset($_POST['username']);
											unset($_POST['password']);
											unset($_POST['password_confirm']);
											unset($_POST['token']);
											unset($_POST['module']);
										} else {
											$_error[] = $msg;
										}
									} # check if register function is callable
								} # token file writing
							} else {
								$_error[] = "Invalid token";
							} # check token
						} # token file reading
					} # error check from password check
				} # check if module file is readable
			} # check if module is registered
		} # token submitted
	} # $_POST data submitted
} # config check
					
?>
<html>
<head>
	<title><?php echo $cfg['title']; ?></title>
	<link rel="stylesheet" href="css/default.css">
</head>

<body>
<h1><?php echo $cfg['title']; ?></h1>

<?php
	# show error messages
	if (count($_error) > 0) {
?>
<div id="error">
<?php
		foreach ($_error as $errmsg) {
			echo  $errmsg . "<br>";
		}
?>
<br>
<span class="contact">Contact <a href="mailto:<?php echo $cfg['admin_email']; ?>">server admin</a> for support</span>
</div>
<br>
<?php

	}
?>

<?php
	# show success messages
	if ($_success) {
?>
<span id="success">
<?php
		echo  $_success. "<br>";
?>
</span>
<br>
<?php

	}
?>

<form action="index.php" method="POST">
	Username:<br>
	<input type="text" name="username" value="<?php echo $_POST['username'] ?$_POST['username'] :''; ?>"><br>
	<br>

	Password:<br>
	<input type="password" name="password"><br>
	<br>

	Confirm Password:<br>
	<input type="password" name="password_confirm"><br>
	<br>

<?php
	switch (count($cfg['modules'])) {
		case 0:
			break;
		case 1:
			echo "	<input type=\"hidden\" name=\"module\" value=\"" . array_keys($cfg['modules'])[0] . "\">";
			break;
		default:
			echo "	Application:<br>\n";
			echo "	<select name=\"module\">\n";
			foreach ($cfg['modules'] as $module_name => $module_desc) {
				echo "		<option value=\"" . $module_name . "\">". $module_desc . "</option>\n";
			}
			echo "	</select><br>\n";
			echo "	<br>\n";
	}
?>

	Token:<br>
	<input type="text" name="token" value="<?php echo $_POST['token'] ?$_POST['token'] :''; ?>"><br>
	<br>

	<br>
	<input type="submit" value="Register">
</form>

</body>
</html>
